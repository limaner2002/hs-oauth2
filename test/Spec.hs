{-# LANGUAGE OverloadedStrings #-}

import Data.Functor.Identity
import Network.OAuth2
import Data.Text (Text)
import Control.Lens
import Control.Monad.Trans.State

main :: IO ()
main = print $ fst $ runIdentity $ runStateT (makeRequestStateT testRequest) testHandle

fetchAccessToken :: ExchangeToken -> Identity (Result Text OAuth2Token)
fetchAccessToken "Good!" = pure $ Right $
  OAuth2Token
    "Granted!"
    (Just "With refresh!")
    (Just 10)
    (Just "Excellent token!")
    (Just "my id")
fetchAccessToken _ = pure $ Left "Bad access token!"

refreshAccessToken :: RefreshToken -> Identity (Result Text OAuth2Token)
refreshAccessToken "With refresh!" = pure $ Right $
  OAuth2Token
    "Granted!"
    (Just "With refresh!")
    (Just 10)
    (Just "Excellent token!")
    (Just "my id")
refreshAccessToken _ = pure $ Left "Bad refresh token!"

checkExpired :: Maybe Int -> Bool
checkExpired Nothing = True
checkExpired (Just 0) = True
checkExpired _ = False

updateExpires :: Maybe Int -> Maybe Int
updateExpires Nothing = Nothing
updateExpires (Just 0) = Nothing
updateExpires (Just n) = Just (n-1)

testHandle :: OAuthHandle Identity Text
testHandle = OAuthHandle fetchAccessToken refreshAccessToken checkExpired "blah!" updateExpires Nothing "Good!"

testRequest :: OAuth2Token -> Identity (Result Text Text)
testRequest tok = pure $ Right $ "Successfully downloaded the resource! :D"
