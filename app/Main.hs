{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

import ClassyPrelude
import Spotify.Types
import Spotify.Mix
import Spotify
import Options.Applicative hiding (Success)
import Spotify.Elastic
import Spotify.Elastic.Endpoints
import Servant.Client (runClientM)
import Data.Aeson.Types (Result (..))

runAddMixTracks :: FilePath -> FilePath -> FilePath -> Int -> SpotifyId -> IO ()
runAddMixTracks tokenPath confPath desiredNamesPath numTracks targetPlaylistId = do
  result <- runSpotifyClient tokenPath confPath $ addMixTracks desiredNamesPath numTracks targetPlaylistId

  case result of
    Left servantError -> print servantError
    Right (Left err) -> putStrLn $ pack err
    Right (Right _) -> putStrLn "Mix created and playlist updated!"

parserInfo :: ParserInfo (IO ())
parserInfo = info (helper <*> parser)
  (  fullDesc
  <> progDesc "A small client for Spotify that is used for creating various mixes."
  )

parser :: Parser (IO ())
parser = runAddMixTracks
  <$> strOption
  (  short 't'
  <> long "token-path"
  <> help "The path to the file that contains the oauth2 token."
  )
  <*> strOption
  (  short 'c'
  <> long "config-path"
  <> help "The path to the oauth2 configuration for the spotify client."
  )
  <*> strOption
  (  short 's'
  <> long "source-playlists"
  <> help "The path to the .json file that contains a list of playlist names to get the source tracks for creating the mix from."
  )
  <*> option auto
  (  short 'n'
  <> long "num-tracks"
  <> help "The number of tracks to add to the target playlist"
  )
  <*> (SpotifyId . pack
       <$> strOption
       (  short 't'
       <> long "target-playlist"
       <> help "The spotify id of the playlist to add the tracks to."
       )
      )

storeAllMyTracks :: FilePath -> FilePath -> IO ()
storeAllMyTracks tokenPath confPath = do
  env <- elasticClientEnv
  let f st = runClientM (putSimplifiedTrack st) env
  res <- runSpotifyClient tokenPath confPath $ do
    myPlaylists <- fetchAllPages spotifyMyPlaylists
    mapM (storePlaylistTracks followPlaylistObjectLink (liftIO . f)) myPlaylists
  case res of
    Left err -> print err
    Right x -> case x of
      Left err -> print err
      Right y -> do
        let successes = length $ rights results
            errors = length $ lefts results
            results = join y
        putStrLn $ tshow successes <> " inserted successfully!"
        putStrLn $ tshow errors <> " errors occurred!"

fetchAllAudioFeatures :: FilePath -> FilePath -> IO ()
fetchAllAudioFeatures tokenPath confPath = do
  env <- elasticClientEnv
  let f features = runClientM (putAudioFeatures features) env
      g = fetchAudioFeatures spotifyAudioFeatures (liftIO . f)
      h v _ = do
        res <- runSpotifyClient tokenPath confPath $
          case decodeHits v of
            Error err -> print err
            Success l -> do
              results <- g $ fmap _simplifiedTrackId l
              let successes = length $ rights results
                  errors = length $ lefts results
              case errors of
                0 -> putStrLn $ tshow successes <> " inserted successfully!"
                _ -> error $ show results
        either print (const $ pure ()) res

  _ <- runClientM (scrollSimplifiedTracks h () "1m") env
  pure ()

main :: IO ()
main = join $ execParser parserInfo
