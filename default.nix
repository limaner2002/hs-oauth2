{ pkgs ? import <nixpkgs> {}
}:
let
  pkg = pkgs.haskellPackages.callCabal2nix "hs-oauth2" ./. { gargoyle-postgresql-connect = gargoyle-packages.gargoyle-postgresql-connect;
      					   	       	     gildar-utilities = gildar-utilities;
      					   	       	   };
  pkg1 = pkg.overrideAttrs ( oldAttrs: rec { nativeBuildInputs = [ pkgs.llvm_6 ] ++ pkg.nativeBuildInputs; });
  ghc = pkgs.haskellPackages.ghcWithPackages (hp: pkg1.passthru.getBuildInputs.haskellBuildInputs ++ [ hp.lens-aeson ] );
  nix-conf_src = fetchGit
                    { url = "ssh://git@gitlab.com/limaner2002/nix-configs.git";
                      rev = "2e9a7c86a434a5a47a7cddffa211caa08b5f9533";
                    };
  gargoyle-packages = pkgs.callPackage "${nix-conf_src}/gargoyle/gargoyle.nix" { callCabal2nix = pkgs.haskellPackages.callCabal2nix; };
  gildar-utilities = pkgs.callPackages "${nix-conf_src}/gildar-utilities/default.nix" { callCabal2nix = pkgs.haskellPackages.callCabal2nix; };
in
  { shell = 
    pkgs.mkShell
      { name = "shell";
        buildInputs = [ ghc pkgs.cabal-install ];
      };
    exe = pkg1;        
  }
