{ mkDerivation, aeson, base, base64-bytestring, binary, bytestring
, classy-prelude, exceptions, foldl, hpack, http-client
, http-client-tls, http-types, lens, MonadRandom, mtl, open-browser
, optparse-applicative, random, random-shuffle, retry, servant
, servant-client, servant-client-core, servant-server, stdenv, text
, time, transformers, uri-bytestring, uri-bytestring-aeson, warp
}:
mkDerivation {
  pname = "hs-oauth2";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base base64-bytestring binary bytestring classy-prelude
    exceptions foldl http-client http-client-tls http-types lens
    MonadRandom mtl open-browser random random-shuffle retry servant
    servant-client servant-client-core servant-server text time
    transformers uri-bytestring uri-bytestring-aeson warp
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    aeson base base64-bytestring binary bytestring classy-prelude
    exceptions foldl http-client http-client-tls http-types lens
    MonadRandom mtl open-browser optparse-applicative random
    random-shuffle retry servant servant-client servant-client-core
    servant-server text time transformers uri-bytestring
    uri-bytestring-aeson warp
  ];
  preConfigure = "hpack";
  homepage = "https://github.com/githubuser/hs-oauth2#readme";
  license = stdenv.lib.licenses.bsd3;
}
