{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module Network.OAuth2.Types where

import ClassyPrelude
import Data.Aeson
import Control.Lens
import Control.Monad.Error.Class
import URI.ByteString
import URI.ByteString.Aeson ()

newtype ExchangeToken = ExchangeToken Text
  deriving (Show, FromJSON, ToJSON)

newtype RefreshToken = RefreshToken Text
  deriving (Show, FromJSON, ToJSON)

newtype AccessToken = AccessToken
  { _unAccessToken :: Text
  } deriving (Show, FromJSON, ToJSON)

newtype IdToken = IdToken Text
  deriving (Show, FromJSON, ToJSON)

data OAuth2Token expiration = OAuth2Token
  { _oauthAccessToken :: AccessToken
  , _oauthRefreshToken :: Maybe RefreshToken
  , _oauthExpiresIn :: expiration
  , _oauthTokenType :: Maybe Text
  , _oauthIdToken :: Maybe IdToken
  } deriving (Show, Generic)

instance ToJSON expiration => ToJSON (OAuth2Token expiration)
instance FromJSON expiration => FromJSON (OAuth2Token expiration)

data OAuthHandle grantType expiration m err = OAuthHandle
  { _grantTokenRequest :: grantType
  , _checkExpired :: expiration -> m Bool
  , _authConf :: OAuth2
  , _updateExpires :: expiration -> m expiration
  , _oauth2Token :: Maybe (OAuth2Token expiration) -- If this is Nothing then it should go the request access flow
  , _exchangeToken :: Maybe ExchangeToken
  }

newtype ClientId = ClientId
  { _clientIdTxt :: Text
  } deriving (Show, ToJSON, FromJSON, IsString, Eq, Ord)

newtype ClientSecret = ClientSecret
  { _clientSecretTxt :: Text
  } deriving (Show, ToJSON, FromJSON, IsString, Eq, Ord)

data OAuth2 = OAuth2
  { _oauthClientId :: Text
  , _oauthClientSecret :: Text
  , _oauthAuthorizeEndpoint :: URI
  , _oauthAccessTokenEndpoint :: URI
  , _oauthCallback :: Maybe URI -- Not sure if I want to hold the callback here or put it in the `AuthorizationCode` type.
  , _oauthScopes :: [Scope]
  } deriving (Show, Eq, Generic)

newtype Scope = Scope
  { _scopeTxt :: Text
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

instance ToJSON OAuth2
instance FromJSON OAuth2

data AuthorizationCode expiration m err = AuthorizationCode
  { _authorizationCodeRequestFn :: MonadError err m => OAuth2 -> m ExchangeToken
  , _authorizationCodeFetchAccessTokenFn :: MonadError err m => ExchangeToken -> m (OAuth2Token expiration)
  , _authorizationCodeRefreshAccessTokenFn :: MonadError err m => RefreshToken -> m (OAuth2Token expiration)
--  , _authorizationCodeRedirectURI :: Maybe URI
  }

newtype ClientCredentials expiration m err = ClientCredentials
  { _clientCredentialsFetchAccessTokenFn :: MonadError err m => ClientId -> ClientSecret -> m (OAuth2Token expiration)
  }

makeLenses ''ClientId
makeLenses ''ClientSecret
makeLenses ''OAuth2Token
makeLenses ''OAuthHandle
makeLenses ''OAuth2
makeLenses ''AccessToken
makeLenses ''AuthorizationCode
makeLenses ''ClientCredentials
makeLenses ''Scope
