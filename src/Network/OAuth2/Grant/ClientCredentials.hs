{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}

module Network.OAuth2.Grant.ClientCredentials where

import ClassyPrelude
       -- Servant imports
import Network.OAuth2.Servant hiding (tokenRequest, requestToken)
import Network.OAuth2 (isExpired)
import Network.OAuth2.Types hiding (checkExpired)
import Servant.Client
import Servant.API
import Data.Proxy
import Control.Lens
import Control.Monad.Reader
import Control.Monad.Error.Class
import Control.Monad.Trans.State

getToken :: MonadError err m => OAuthHandle (ClientCredentials expiration m err) expiration m err -> m (OAuth2Token expiration)
getToken handle =
  case handle ^. oauth2Token of
    Nothing -> do
      grantFetchToken cid secret
    Just token -> do
      expired <- isExpired handle
      case expired of
        True -> do
          grantFetchToken cid secret
        False -> pure token

  where
    grantFetchToken = handle ^. grantTokenRequest . clientCredentialsFetchAccessTokenFn
    cid = handle ^. authConf . oauthClientId . to ClientId
    secret = handle ^. authConf . oauthClientSecret . to ClientSecret

makeRequest :: MonadError err m => OAuthHandle (ClientCredentials expiration m err) expiration m err -> (OAuth2Token expiration -> m a) -> m (a, OAuthHandle (ClientCredentials expiration m err) expiration m err)
makeRequest handle requestFcn = do
  tok <- getToken handle
  a <- requestFcn tok
  let handle' = handle & oauth2Token ?~ tok
  pure (a, handle')

makeRequestStateT :: MonadError err m => (OAuth2Token expiration -> m a) -> StateT (OAuthHandle (ClientCredentials expiration m err) expiration m err) m a
makeRequestStateT f = do
  handle <- get
  (a, handle') <- lift $ makeRequest handle f
  put handle'
  pure a

       -- Servant stuff below

type ClientCredentialsTokenEndpoint = ReqBody '[FormUrlEncoded] (Map Text [Text]) :> OAuthFetchAccessToken

tokenRequest :: OAuth2 -> ClientId -> ClientSecret -> ClientM Response
tokenRequest oauth cid secret = local (setTokenHost oauth) $ tokenRequest' bodyParams oauth "GET"
  where
    tokenRequest' = client (Proxy :: Proxy ClientCredentialsTokenEndpoint)
    bodyParams = addCid . addSecret . setGrant $ mempty
    addCid = at "client_id" ?~ [cid ^. clientIdTxt]
    addSecret = at "client_secret" ?~ [secret ^. clientSecretTxt]
    setGrant = at "grant_type" ?~ ["client_credentials"]

type ClientCredentialsHandle m err = OAuthHandle (ClientCredentials (Maybe UTCTime) m err) (Maybe UTCTime) m err

requestToken :: OAuth2 -> ClientId -> ClientSecret -> ClientM ServantOAuth2Token
requestToken oauth cid secret = do
  resp <- tokenRequest oauth cid secret
  decodeSpotifyToken resp

servantHandle :: OAuth2 -> Maybe ServantOAuth2Token -> ClientCredentialsHandle ClientM ClientError
servantHandle oauth mTok = OAuthHandle grantType (maybe (pure True) checkExpired) oauth pure mTok Nothing
  where
    grantType = ClientCredentials (requestToken oauth)
