{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Network.OAuth2.Servant where

import Servant.API hiding (URI, uriQuery, header)
import Servant.Client
import Servant.Client.Core
import Servant.Server
import Network.Wai.Handler.Warp
import Network.OAuth2
import Network.OAuth2.Types hiding (checkExpired)
import Data.Proxy
import qualified Network.HTTP.Types as H
import Control.Lens
import URI.ByteString
import ClassyPrelude hiding (Handler)
import qualified Data.ByteString.Builder as BS
import qualified Network.HTTP.Client as C
import qualified Network.HTTP.Client.TLS as TLS
import Web.Browser
import qualified Data.ByteString.Base64 as Base64
import Data.Aeson hiding (Result)
import Control.Monad.Reader
import Control.Monad.Error.Class
       -- Only for testing
import Data.Time (addUTCTime)

data OAuthFetchAccessToken

type ServantOAuth2Token = OAuth2Token (Maybe UTCTime)
type ServantOAuthHandle m err = OAuthHandle (AuthorizationCode (Maybe UTCTime) m err) (Maybe UTCTime) m err

instance RunClient m => HasClient m OAuthFetchAccessToken where
  type Client m OAuthFetchAccessToken
    = OAuth2 -> H.Method -> m Response

  clientWithRoute :: Proxy m -> Proxy OAuthFetchAccessToken -> Request -> Client m OAuthFetchAccessToken
  clientWithRoute _ _ req oauth httpMethod =
    runRequest req { requestMethod = httpMethod
                   , requestPath = oauth ^. oauthAccessTokenEndpoint . pathL . to toBuilder
                   }

type AuthEndpoint = QueryParam "code" Text :> QueryParam "state" Text :> Get '[OctetStream] NoContent
type ReturnStatus = Get '[JSON] Bool
type ServerAPI = AuthEndpoint :<|> ReturnStatus

type TokenEndpoint = ReqBody '[FormUrlEncoded] (Map Text [Text]) :> Header "Authorization" Text :> OAuthFetchAccessToken
type RefreshEndpoint = ReqBody '[FormUrlEncoded] (Map Text [Text]) :> Header "Authorization" Text :> OAuthFetchAccessToken
type AuthorizedRequest = Header "Authorization" ServantOAuth2Token

instance ToHttpApiData ServantOAuth2Token where
  toUrlPiece tok = tok ^. oauthAccessToken . unAccessToken

  toHeader tok = header
    where
      header = "Bearer " <> (tok ^. oauthAccessToken . unAccessToken . to encodeUtf8)

-- | Once the authorization code is received, the server is
-- stopped. However the server stops before sending anything to the
-- browser so it looks like an error occurred. It would be better if
-- something could be sent before killing the server.
authorize :: MVar (Maybe ExchangeToken) -> Maybe Text -> Maybe Text -> Handler NoContent
authorize toDie (Just code) _ = do
  print code
  putMVar toDie $ Just $ ExchangeToken code
  pure NoContent
authorize toDie Nothing _ = do
  putStrLn "No code? WTF!?"
  putMVar toDie Nothing
  pure NoContent

setClientEnvHost :: String ->  ClientEnv -> ClientEnv
setClientEnvHost hostName env = env'
  where
    env' = env { baseUrl = baseUrl' $ baseUrl env }
    baseUrl' base = base { baseUrlHost = hostName }

setTokenHost :: OAuth2 -> ClientEnv -> ClientEnv
setTokenHost oauth env
  = case oauth ^? oauthAccessTokenEndpoint . to URI.ByteString.uriAuthority . traverse . to (hostBS . authorityHost) of
      Nothing -> env
      Just host -> setClientEnvHost (unpack $ decodeUtf8 host) env

tokenRequest :: OAuth2 -> ExchangeToken -> ClientM Response
tokenRequest oauth (ExchangeToken tok) = local (setTokenHost oauth) $ tokenRequest' bodyParams (Just idSecret) oauth "POST"
  where
    tokenRequest' = client (Proxy :: Proxy TokenEndpoint)
    bodyParams = addToken . setGrant . addRedirect $ mempty
    addToken = at "code" ?~ [tok]
    setGrant = at "grant_type" ?~ ["authorization_code"]
    addRedirect = at "redirect_uri" ?~ (oauth ^.. oauthCallback . traverse . to (decodeUtf8 . serializeURIRef'))
    idSecret = decodeUtf8 $ "Basic " <> Base64.encode (encodeUtf8 $ (oauth ^. oauthClientId) <> ":" <> (oauth ^. oauthClientSecret))

tokenRefresh :: OAuth2 -> RefreshToken -> ClientM Response
tokenRefresh oauth (RefreshToken tok) = local (setTokenHost oauth) $ tokenRefresh' bodyParams (Just idSecret) oauth "POST"
  where
    tokenRefresh' = client (Proxy :: Proxy RefreshEndpoint)
    bodyParams = addToken . setGrant $ mempty
    addToken = at "refresh_token" ?~ [tok]
    setGrant = at "grant_type" ?~ ["refresh_token"]
    idSecret = decodeUtf8 $ "Basic " <> Base64.encode (encodeUtf8 $ (oauth ^. oauthClientId) <> ":" <> (oauth ^. oauthClientSecret))

server :: MVar (Maybe ExchangeToken) -> Application
server toDie = serve (Proxy :: Proxy ServerAPI) (authorize toDie :<|> returnStatus)

returnStatus :: Handler Bool
returnStatus = pure True

dispRequest :: RequestF BS.Builder BS.Builder -> String
dispRequest req = path <> "\n"
  <> show method <> "\n"
  <> show query <> "\n"
  where
    path = unpack $ toStrict $ decodeUtf8 $ builderToLazy $ requestPath req
    method = requestMethod req
    query = requestQueryString req

clientEnvWith :: (C.Request -> IO C.Request) -> String -> IO (Maybe (TVar C.CookieJar) -> ClientEnv)
clientEnvWith reqLoggerFunc url = do
  mgr <- C.newManager settings
  return $ ClientEnv mgr (BaseUrl Https url 443 "")
    where
      settings = TLS.tlsManagerSettings { C.managerModifyRequest = reqLoggerFunc
                                        }
authorizeBrowser :: MVar () -> OAuth2 -> IO Bool
authorizeBrowser waitStart oauth = do
  _ <- takeMVar waitStart
  openBrowser authUrl
  where
    authUrl = oauth ^. oauthAuthorizeEndpoint . to (unpack . decodeUtf8 . serializeURIRef' . addQueryParams params)
    params :: [(ByteString, ByteString)]
    params = [ ("client_id", oauth ^. oauthClientId . to encodeUtf8)
             , ("response_type", "code")
             , ("redirect_uri", oauth ^. oauthCallback . traverse . to serializeURIRef')
             , ("scope", scopes)
             ]
    scopes = intercalate " " $ oauth ^.. oauthScopes . traverse . scopeTxt . to encodeUtf8

authorizeUrl :: OAuth2 -> String
authorizeUrl oauth = oauth ^. oauthAuthorizeEndpoint . to (unpack . decodeUtf8 . serializeURIRef' . addQueryParams params)
  where
    params :: [(ByteString, ByteString)]
    params = [ ("client_id", oauth ^. oauthClientId . to encodeUtf8)
             , ("response_type", "code")
             , ("redirect_uri", oauth ^. oauthCallback . traverse . to serializeURIRef')
             ]

requestAuthorization :: OAuth2 -> IO ExchangeToken
requestAuthorization oauth = do
  mv <- newEmptyMVar
  (_, token) <- concurrently (authorizeBrowser mv oauth) (startServer mv)
  pure token

requestToken :: OAuth2 -> ExchangeToken -> ClientM ServantOAuth2Token
requestToken oauth excToken = do
  resp <- tokenRequest oauth excToken
  decodeSpotifyToken resp

decodeSpotifyToken :: (MonadIO m, MonadError ClientError m) => Response -> m ServantOAuth2Token
decodeSpotifyToken resp = do
  let lbs = responseBody resp

  currentTime <- liftIO getCurrentTime
  
  case _spotifyToken <$> eitherDecode lbs of
    Left err -> throwError $ DecodeFailure (pack err) resp
    Right tok ->
      case tok ^? oauthExpiresIn . traverse of
        Nothing -> throwError $ DecodeFailure "No expiration time in the response body found!" resp
        Just expiration -> pure $ oauthExpiresIn ?~ (addUTCTime (fromIntegral expiration) currentTime) $ tok

refreshTokenRequest :: OAuth2 -> RefreshToken -> ClientM ServantOAuth2Token
refreshTokenRequest oauth refreshToken = do
  resp <- tokenRefresh oauth refreshToken
  tok <- decodeSpotifyToken resp
  pure $ tok & oauthRefreshToken %~ maybe (Just refreshToken) Just  

startServer :: MVar () -> IO ExchangeToken
startServer startBrowser = do
  mv <- newEmptyMVar
  res <- race (run 4242 (server mv)) (checkStatus startBrowser mv)
  case res of
    Left _ -> fail "The server quit unexpectedly!"
    Right (Just tok) -> pure tok
    Right Nothing -> fail "There was no token!"

    -- Should check the server to make sure it's up and then notify
    -- the thread that it's ok to start the browser.
checkStatus :: MVar () -> MVar (Maybe ExchangeToken) -> IO (Maybe ExchangeToken)
checkStatus startBrowser token = do
  putMVar startBrowser ()
  takeMVar token

addQueryParams :: [(ByteString, ByteString)] -> URI -> URI
addQueryParams params uri = uri { uriQuery = Query params }

newtype SpotifyOAuth2Token = SpotifyOAuth2Token
  { _spotifyToken :: OAuth2Token (Maybe Int)
  } deriving Show

instance FromJSON SpotifyOAuth2Token where
  parseJSON (Object o) = SpotifyOAuth2Token <$> (OAuth2Token
    <$> o .: "access_token"
    <*> o .:? "refresh_token"
    <*> o .:? "expires_in"
    <*> o .:? "token_type"
    <*> o .:? "id_token"
                                                )
  parseJSON _ = fail "Expecting object when decoding Spotify auth2 token."

servantHandle :: OAuth2 -> Maybe ServantOAuth2Token -> ServantOAuthHandle ClientM ClientError
servantHandle oauth mTok = OAuthHandle grantType (maybe (pure True) checkExpired) oauth pure mTok Nothing
  where
    grantType = AuthorizationCode (liftIO . requestAuthorization) (requestToken oauth) (refreshTokenRequest oauth) 

checkExpired :: MonadIO m => UTCTime -> m Bool
checkExpired time = do
  ct <- liftIO getCurrentTime
  pure $ time < ct

