{-# LANGUAGE NoImplicitPrelude #-}

module Network.OAuth2
  ( OAuth2Token (..)
  , OAuthHandle (..)
  , makeRequest
  , makeRequestStateT
  , RefreshToken (..)
  , ExchangeToken (..)
  , oauthAccessToken
  , unAccessToken
  , OAuth2
  , oauthClientId
  , oauthClientSecret
  , oauthAuthorizeEndpoint
  , oauthAccessTokenEndpoint
  , oauthCallback
  , authConf
  , oauthIdToken
  , oauthTokenType
  , updateExpires
    -- Debug
  , getToken
  , getAuthorization
  , oauth2Token
  , oauthExpiresIn
  , oauthRefreshToken
    -- Perhaps needed?
  , isExpired
  ) where

import ClassyPrelude hiding (handle)
import Data.Text (Text)
import Control.Lens
import Control.Monad.Trans.State
import Control.Monad.Trans.Class ()
import URI.ByteString
import Data.Aeson hiding (Result)
import GHC.Generics ()
import Control.Monad.Error.Class
import Network.OAuth2.Types

-- | Request authorization from the OAuth2 provider.
getAuthorization :: MonadError err m => OAuthHandle (AuthorizationCode expiration m err) expiration m err -> m ExchangeToken
getAuthorization handle = case handle ^. exchangeToken of
  Just tok -> pure tok
  Nothing -> getAuthorization' $ handle ^. authConf
 where
   getAuthorization' = handle ^. grantTokenRequest . authorizationCodeRequestFn

-- | Request a token from the OAuth2 provider. If the token in the
-- handle is present and is not expired it is used. Otherwise if
-- the token is expired then it either refreshes the token or
-- fetches a new token depending on whether or not the refresh
-- token is present.
getToken :: MonadError err m => OAuthHandle (AuthorizationCode expiration m err) expiration m err -> m (OAuth2Token expiration)
getToken handle =
  case handle ^. oauth2Token of
    Nothing -> do
      excgTok <- getAuthorization handle
      fetchToken excgTok
    Just token -> do
      expired <- isExpired handle
      case expired of
        True -> case token ^. oauthRefreshToken of
          Nothing -> do
            excgTok <- getAuthorization handle
            fetchToken excgTok
          Just rTok -> refresh rTok
        False -> pure token
 where
   fetchToken = handle ^. grantTokenRequest . authorizationCodeFetchAccessTokenFn
   refresh = handle ^. grantTokenRequest . authorizationCodeRefreshAccessTokenFn

-- | Make a request to an api endpoint that requires OAuth2
-- authorization. This function will use the handle for fetching
-- and/or refreshing the token as well as applying the function to
-- check whether or not the current token is expired.
makeRequest :: MonadError err m => OAuthHandle (AuthorizationCode expiration m err) expiration m err -> (OAuth2Token expiration -> m a) -> m (a, OAuthHandle (AuthorizationCode expiration m err) expiration m err)
makeRequest handle requestFcn = do
  tok <- getToken handle
  a <- requestFcn tok
  let handle' = handle & oauth2Token ?~ tok
  pure (a, handle')

-- | If the OAuth2 token is present then applies the check expiration
-- function in the handle, otherwise returns `False`
isExpired :: Applicative m => OAuthHandle grantType expiration m err -> m Bool
isExpired handle =
  case handle ^? oauth2Token . traverse . oauthExpiresIn of
    Nothing -> pure True
    Just expires -> check expires
  where
    check = handle ^. checkExpired

-- | Same as `makeRequest` storing the handle in the `StateT` transformer.
makeRequestStateT :: MonadError err m => (OAuth2Token expiration -> m a) -> StateT (OAuthHandle (AuthorizationCode expiration m err) expiration m err) m a
makeRequestStateT f = do
  handle <- get
  (a, handle') <- lift $ makeRequest handle f
  put handle'
  pure a
