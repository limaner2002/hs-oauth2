# hs-oauth2

An experimental OAuth2 client library that is meant to address the limitations of the exesting Haskell OAuth2 libraries. This library intends to be backend agnostic. Currently only the `Servant` client backend is implemented and only tested with the Spotify Web API. This library is very experimental and not ready for prime time use yet.